import sqlite3 
from flask import Flask, request, jsonify, escape
app = Flask(__name__)
app.debug = True
try:
    
    conn = sqlite3.connect('HW2_redo.db')
    c = conn.cursor()
    conn.execute("PRAGMA foreign_keys = ON")
    
    # create a table for idIndex
    c.execute("""
    DROP TABLE IF EXISTS MainId""")
    
    c.execute("""
    CREATE TABLE MainId (idd TEXT, site TEXT, PRIMARY KEY ('idd'))
    """)
    
    c.execute("""INSERT INTO MainId VALUES ('ValSten','South America')""")
    c.execute("""INSERT INTO MainId VALUES ('BatSten','South America')""")
    c.execute("""INSERT INTO MainId VALUES ('IpaCor','Asia')""")
    
    # create a table for color: 
    c.execute("""DROP TABLE IF EXISTS color""")
    
    c.execute("""CREATE TABLE color (idd TEXT, color TEXT, colorR INT, colorG INT, colorB INT, sizeCM INT,
    FOREIGN KEY ('idd')
    REFERENCES MainId('idd')
    ON DELETE CASCADE
    ON UPDATE CASCADE)""")
    
    c.execute("""INSERT INTO color VALUES ('ValSten','red', 99, 37, 12, 5)""")
    c.execute("""INSERT INTO color VALUES('BatSten','black', 28, 23, 1, 4)""")
    c.execute("""INSERT INTO color VALUES('IpaCor','brown', 156, 111, 0, 5)""")
    c.execute("""INSERT INTO color VALUES ('ValSten','black', 28, 23, 1, 3)""")
    
    
    # create a table lineage
    c.execute("""DROP TABLE IF EXISTS lineage""")
    
    c.execute("""CREATE TABLE lineage (idd TEXT, mother TEXT, father TEXT,
    FOREIGN KEY ('idd')
    REFERENCES MainId('idd')
    ON DELETE CASCADE
    ON UPDATE CASCADE)""")
    
    c.execute("""INSERT INTO lineage VALUES ('ValSten','valida', 'stenosperma')""")
    c.execute("""INSERT INTO lineage VALUES ('BatSten','batizocoi', 'stenosperma')""")
    c.execute("""INSERT INTO lineage VALUES ('IpaCor','ipaënsis', 'correntina')""")
    
    # number of seed in WPL Inventory
    c.execute("""DROP TABLE IF EXISTS Inventory""")
    
    c.execute("""CREATE TABLE Inventory (idd TEXT, number INT, 
    FOREIGN KEY ('idd')
    REFERENCES MainId('idd')
    ON DELETE CASCADE
    ON UPDATE CASCADE)""")
    
    c.executemany("""
    INSERT INTO Inventory VALUES (?,?)""",[
    ('ValSten',45),
    ('BatSten',379),
    ('IpaCor',73)
    ])
    
    #create a table for reference gene
    c.execute("""DROP TABLE IF EXISTS gene""")
    
    c.execute("""CREATE TABLE gene (idd TEXT, gene TEXT)""")
    
    c.executemany("""
    INSERT INTO gene VALUES  (?,?)""",[
    ('ValSten','DRO3'), ('ValSten','SRA15'),
    ('BatSten','LFS3'), ('BatSten','SRA15'),
    #('IpaCor','SRA15'), ('IpaCor','R7')
    ])
    
    conn.commit();
    
    
finally:
    print()
    print("This is executed")
    conn.close()


@app.route('/', methods=['GET'])
def fun0():
    return jsonify("HW3 gene welcome") 


@app.route('/GetGene')
def fun2():
    connection = sqlite3.connect('HW2_redo.db')
    cursor = connection.cursor()
    cursor = connection.execute("SELECT idd, gene from gene")
    records = cursor.fetchall()
    api_list=[]
    for row in records:
        a_dict={}
        a_dict['idd']=row[0]
        a_dict['gene']=row[1]
        api_list.append(a_dict)
    connection.close()
    return jsonify(api_list)



@app.route('/AddGene/', methods=['GET', 'POST']) 
def add1():   
    idd = request.args.get("idd","fail to add")
    gene = request.args.get("gene", "fail to add")
    connection = sqlite3.connect('HW2_redo.db')
    cursor = connection.cursor()
    cursor = connection.execute("""INSERT INTO gene (idd, gene) VALUES(?,?);""", (idd, gene))
    connection.commit()
    connection.close()
    response = {'idd':idd, 'site':gene}
    
    return jsonify(response), 200

@app.route('/GetSpeGene/', methods=['GET'])
def getspe():
    idd = request.args.get("idd")
    connection = sqlite3.connect('HW2_redo.db')
    cursor = connection.cursor()
    cursor = connection.execute("""SELECT * From gene WHERE idd=?""", (idd,))
    rows = cursor.fetchall()
    api_list=[]
    for row in rows:
        a_dict={}
        a_dict['idd']=row[0]
        a_dict['gene']=row[1]
        api_list.append(a_dict)
    connection.close()
    return jsonify(api_list)
    return jsonify(row)


